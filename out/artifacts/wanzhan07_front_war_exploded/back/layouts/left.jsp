<%--
  Created by IntelliJ IDEA.
  User: CDLX
  Date: 2020/7/20
  Time: 14:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../../css/back/layouts/left.css">
</head>
<body>
<div class="left">
    <div>
        <div>用户管理</div>
        <div><a href="javascript:$('#content').load('/back/user/userManagement.jsp')">用户查询</a></div>
        <div><a href="javascript:$('#content').load('/back/user/userAdd.jsp')">用户添加</a></div>
    </div>

    <div>
        <div>项目类型</div>
        <div><a href="javascript:$('#content').load('/back/project/typeCheck.jsp')">类型查询</a></div>
        <div><a href="javascript:$('#content').load('/back/project/typeAdd.jsp')">类型添加</a></div>
    </div>

    <div>
        <div>项目管理</div>
        <div><a href="javascript:$('#content').load('/back/project/projectAll.jsp')">项目查询</a></div>
        <div><a href="javascript:$('#content').load('/back/project/projectAdd.jsp')">项目新增</a></div>
    </div>

    <div>
        <div>导航管理</div>
        <div><a href="javascript:$('#content').load('/back/navs/navAll.html')">导航查询</a></div>
        <div><a href="javascript:$('#content').load('/back/navs/navAdd.html')">导航新增</a></div>
    </div>



    <div>
        <div>新闻管理</div>
        <div>新闻查询</div>
        <div>新闻新增</div>
    </div>

    <div>
        <div>招聘管理</div>
        <div>招聘查询</div>
        <div>招聘新增</div>
    </div>


    <div>
        <div>留言管理</div>
        <div>留言查询</div>
        <div>留言修改</div>
    </div>



</div>
</body>
</html>
