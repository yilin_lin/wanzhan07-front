$(function () {
    $("#page").load("/back/page.html");

})
function  findAll() {

    let da={};
    da.companyName=$.trim($("#companyName").val());
    da.isshow=$("input[name='isshow']:checked").val();
    da.toping=$("input[name='toping']:checked").val();
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());
    let data=Myajax("/back/company/findAll",da,"get")
    $("#totalCount").html(data.count);
    data=data.list;
    if(data!=null){
        let html=" ";
        for(let i=0;i<data.length;i++){
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].webName + '</td>\n' +
                '<td><img src="' +data[i].logo +'"style="width: 50px;height: 40px"/> </td>\n' +
                '<td>' + data[i].address + '</td>\n' +
                '<td>' + (data[i].isShow == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].toping == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].createTime==undefined?' ':data[i].createTime) + '</td>\n' +
                '<td>' + (data[i].updateTime ==undefined?' ':data[i].updateTime)+ '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n'+
                '</tr>';
        }
        $("#companyFindAll").html(html);
    }
}

function del(id) {
    let data=Myajax("/back/company/del",{id:id},"post");
    if (data.count==1){
        alert("删除成功");
        findAll();
    } else {
        alert("删除失败")
    }

}

function edit(id){

    localStorage.setItem("companyId",id);
    $("#content").load("/back/company/companyEdit.html");
}
