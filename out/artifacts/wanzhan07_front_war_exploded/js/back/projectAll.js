$(function () {
    $("#page").load("/back/page.html")
});

function findAll() {

    let da={};
    da.projectName = $.trim($("#projectName").val());
    da.typeName = $.trim($("#typeName").val());
    da.show = $("input[name='show']:checked").val();
    da.toping = $("input[name='toping']:checked").val();
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());
    let data = Myajax("/back/project/projectFindAll", da, "get")
    $("#totalCount").html(data.count);
    data = data.list;

    if (data != null) {
        let html = '';
        for (let i = 0; i < data.length; i++) {
            console.log( data[i].creataTime)
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td><img src="' +data[i].img +'"style="width: 50px;height: 40px"/> </td>\n' +
                '<td>' + data[i].projectName + '</td>\n' +
                '<td>' + data[i].description + '</td>\n' +
                // '<td>' + data[i].price + '</td>\n' +
                // '<td>' + data[i].region + '</td>\n' +
                '<td>' + data[i].projectType + '</td>\n' +
                '<td>' + (data[i].createTime==undefined?' ':data[i].createTime) + '</td>\n' +
                '<td>' + (data[i].updateTime ==undefined?' ':data[i].updateTime)+ '</td>\n' +
                '<td>' + (data[i].isShow == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].toping == 1 ? '是' : '否') + '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n'+
            '</tr>';
            $("#projectFindAll").html(html);//对当前页面进行赋值
        }
    } else {
        alert("查询失败")
    }
}

function del(id) {
    let data=Myajax("/back/project/projectdel",{id:id},"post")
    if(data.count==1){
        alert("删除成功");
       findAll();
    }else{
        alert("删除失败");
    }
}

function edit(id){

    localStorage.setItem("projectId",id);
    $("#content").load("/back/project/projectEdit.html");
}