//代表加载页面时执行里面的方法
$(function () {
    $("#page").load("/back/page.html");
});

//通过id对数据进行排序
function orderById() {

    let aa=$("div[orderBy='id']");
    console.log(aa.val()==0);
    if (aa.val()==0){
        aa.val("1");
        aa.css("background-color","red")
    }else{
        aa.val("0");
        aa.css("background-color","white")
    }

    findAll();
}


//查找所有数据
function findAll() {
    let da = {};
    da.username = $("#username").val();
    da.phone = $("#phone").val();
    da.sex = $("input[name='sex']:checked").val();
    da.startBirthday = $("#startBirthday").val();
    da.endBirthday = $("#endBirthday").val();
    da.hobby = JSON.stringify(aihao());
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());
    da.orderById = $("div[orderBy='id']").val();

    let data = Myajax("/back/user/findUser", da, "get")
    $("#totalCount").html(data.count);
    data = data.map.userModelList;
    console.log(data);
    if (data == null) {
        alert("查询失败")
    } else {
        let html = '';//字符串拼接
        for (let i = 0; i < data.length; i++) {
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].username + '</td>\n' +
                '<td>' + data[i].phone + '</td>\n' +
                '<td>' + data[i].sex + '</td>\n' +
                '<td>' + data[i].birthday + '</td>\n' +
                '<td>' + data[i].hobby + '</td>\n' +
                '<td>' + data[i].introduce + '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n'
            '</tr>';

        }
        $("#userFindAll").html(html);//对当前页面进行赋值
    }
}


function edit(id) {
    localStorage.setItem("userid", id);//或者sessionStorage.setItem("userid",id)
    $("#content").load("/back/user/userEdit.html");
}

function del(id) {
    let data = Myajax("/back/user/delete", {id: id}, "post");
    if (data.count == 1) {
        findAll();
    } else {
        alert("删除失败")
    }
}

function aihao() {
    let hb = [];
    $("input:checkbox[name='hobby']").each(function () {
        if ($(this).prop("checked")) {
            hb.push($(this).val());
        }
    })
    return hb;
}

