$(function () {
    $("#page").load("/back/page.html");
});

//得到项目类型
function findAll() {
    let da = {};
    da.typeName = $.trim($("#typeName").val());
    da.show = $("input[name='show']:checked").val();
    da.toping = $("input[name='toping']:checked").val();
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());
    da.typeControl=$("input[name='typeControl']:checked").val();

    let data = Myajax("/back/project/typeFindAll", da, "get");
    $("#totalCount").html(data.count);
    data = data.list;
    if (data != null) {
        let html = '';
        let defaultImg="/loadImg/default.jpg";
        for (let i = 0; i < data.length; i++) {
            console.log(data[0].updateTime);
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].typename + '</td>\n' +
                '<td><img src="' +(data[i].typeImg=="undefined"?defaultImg:data[i].typeImg) +'"style="width: 50px;height: 40px"/> </td>\n' +
                '<td>' + (data[i].isShow == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].toping == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].typeControl == 1 ? '其他页面显示' : '新闻页面显示') + '</td>\n' +
                '<td>' + data[i].createTime + '</td>\n' +
                '<td>' + (data[i].updateTime == undefined ? ' ' : data[i].updateTime) + '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n'
            '</tr>';
            $("#typeFindAll").html(html);//对当前页面进行赋值
        }
    } else {
        alert("查询失败")
    }
}

function del(id) {
    let data = Myajax("/back/projectType/typeDel", {id: id}, "post");
    console.log(data);
    if (data.count == 1) {
        alert("刪除成功");
        findAll();
    } else {
        alert("项目删除失败");
    }
}

function edit(id) {
    localStorage.setItem("typeId", id);
    $("#content").load("/back/project/typeEdit.html")


}