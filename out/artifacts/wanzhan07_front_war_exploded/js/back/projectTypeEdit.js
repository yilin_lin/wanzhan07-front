$(function () {
    findProjectTypeById()
});

function findProjectTypeById(){
    let id=localStorage.getItem("typeId");
    let data=Myajax("/back/projectType/findTypeById",{id:id},"post");
    data=data.projectTypeModel;

    if (data!=null){
        $("#typeName").val(data.typename);
        $("#typeIntroduce").val(data.typeIntroduce);
        if (data.typeImg=="undefined") {
            $("#imgHref").attr("src","/loadImg/default.jpg");
        }else{
            $("#imgHref").attr("src",data.typeImg);
        }


        $("input[name=show][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=toping][value=" + data.toping + "]").attr("checked", true);
        $("input[name=typeControl][value="+data.typeControl+"]").attr("checked",true);
    }else{
        alert("修改失败")
    }
}

function projectTypeEdit() {

    let id=localStorage.getItem("typeId")

    let data = new FormData();
    data.append("typeName", $.trim($("#typeName").val()));
    data.append("typeIntroduce", $.trim($("#typeIntroduce").val()));
    data.append("typeImg", $("#typeImg")[0].files[0]);
    data.append("show", $("input[name='show']:checked").val());
    data.append("toping", $("input[name='toping']:checked").val());
    data.append("id", id);
    data.append("typeControl",$("input[name='typeControl']:checked").val());
    let result=imgAjax("/back/projectType/typeEdit",data,"post");

            if (result.count == 1) {
                alert("编辑成功")
                $("#content").load("/back/project/typeCheck.html")
            } else {
                alert("编辑失败");
            }



}