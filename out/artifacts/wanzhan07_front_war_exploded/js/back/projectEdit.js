$(function () {
    lookType();
    findProject();
});

function findProject() {
    let id=localStorage.getItem("projectId");

    let data=Myajax("/back/project/findProjectById",{id:id},"post");
    data=data.projectModel;
    if (data!=null) {
        $("#projectType option[value="+data.projectTypeId +"]").attr("selected",true);
        $("#projectName").val(data.projectName);
        $("#price").val(data.price);
        $("#region").val(data.region);
        $("#description").val(data.description);
        $("input[name=show][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=toping][value=" + data.toping + "]").attr("checked", true);
        $("#imgHref").attr("src",data.img);
    }

}

//得到项目类型
function lookType() {
    let data = Myajax("/back/project/typeFindAll", {}, "get");
    data = data.list;
    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<option value="'+data[i].id+'">' + data[i].typename + '</option>'

    }
    $("#projectType").html(html);
}

function projectEdit() {
    let id=localStorage.getItem("projectId");
    let data = new FormData();
    data.append("projectType", $("#projectType").val());
    data.append("projectName", $("#projectName").val());
    data.append("price", $("#price").val());
    data.append("region", $("#region").val());
    data.append("description", $("#description").val());
    data.append("show", $("input[name='show']:checked").val());
    data.append("img", $("#img")[0].files[0]);
    data.append("id", id);
    data.append("toping", $("input[name='toping']:checked").val());

    let result=imgAjax("/back/project/edit",data,"post");

            if (result.count == 1) {
                alert("修改成功")
                $("#content").load("/back/project/projectAll.html")
            } else {
                alert("修改失败");
            }



}