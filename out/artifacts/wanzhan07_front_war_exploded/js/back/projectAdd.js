$(function () {
    lookType()
});

//得到项目类型
function lookType() {
    let data = Myajax("/back/project/typeFindAll", {}, "get");
    data = data.list;
    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<option value="' + data[i].id + '">' + data[i].typename + '</option>'

    }
    $("#projectType").html(html);
}

function projectAdd() {

    let data = new FormData();
    data.append("projectType", $("#projectType").val());
    data.append("projectName", $("#projectName").val());
    data.append("price", $("#price").val());
    data.append("region", $("#region").val());
    data.append("description", $("#description").val());
    data.append("show", $("input[name='show']:checked").val());
    data.append("img", $("#img")[0].files[0]);
    data.append("toping", $("input[name='toping']:checked").val());

    let result=imgAjax("/back/project/add",data,"post");

            if (result.count == 1) {
                alert("添加成功")
                $("#content").load("/back/project/projectAll.html")
            } else {
                alert("添加失败");
            }


}