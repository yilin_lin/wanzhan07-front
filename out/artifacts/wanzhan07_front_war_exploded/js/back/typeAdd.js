function checkType() {
    let name = $.trim($("#typeName").val());
    if (name == "") {
        return true;
    } else {
        return false;
    }
}


function typeAdd() {

    if (!checkType()) {

        let data = new FormData();
        data.append("typeName", $("#typeName").val());
        data.append("typeIntroduce", $("#typeIntroduce").val());
        data.append("typeImg", $("#typeImg")[0].files[0]);
        data.append("show", $("input[name='show']:checked").val());
        data.append("toping", $("input[name='toping']:checked").val());
        data.append("typeControl",$("input[name='typeControl']:checked").val());

        let result=imgAjax("/back/project/typeAdd",data,"post");

                if (result.count == 1) {
                    alert("添加成功")
                    $("#content").load("/back/project/typeCheck.html")
                } else {
                    alert("添加失败");
                }

    } else {
        alert("类型名不能为空");
    }

}

