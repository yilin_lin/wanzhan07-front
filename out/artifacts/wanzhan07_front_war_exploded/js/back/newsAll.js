$(function () {
    $("#page").load("/back/page.html")
});

function findAll() {
    let da={}
    da.newsTitle=$.trim($("#newsTitle").val());
    da.newsSummary=$.trim($("#newsSummary").val());
    da.newsDate=$.trim($("#newsDate").val());
    da.isShow=$("input[name='show']:checked").val();
    da.toping=$("input[name='toping']:checked").val();
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());

    let data = Myajax("/back/news/newsAll", da, "get")
    $("#totalCount").html(data.count);
    data = data.list;
    let html = "";
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].newsTitle + '</td>\n' +
                '<td class="newsSummary" title="' + data[i].newsSummary + '">' + data[i].newsSummary + '</td>\n' +
                '<td>' + data[i].newsDate + '</td>\n' +
                '<td>' + (data[i].isShow == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].toping == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].createTime == undefined ? ' ' : data[i].createTime) + '</td>\n' +
                '<td>' + (data[i].updateTime == undefined ? ' ' : data[i].updateTime) + '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n' +
                '</tr>';
        }
    }
    $("#newsFindAll").html(html)
}

function del(id) {
    let data=Myajax("/back/news/del",{id:id},"post");
    console.log(data.count);
    if(data.count==1){
        alert("删除成功");
        findAll();
    }else{
        alert("删除失败");
    }
}
function edit(id) {
    localStorage.setItem("newsId",id);
    $("#content").load("/back/news/newsEdit.html");
}