$(function () {
    findAdvantageById();
});

function findAdvantageById() {
   let id=localStorage.getItem("advantageId");
    let data=Myajax("/back/advantage/findAdvantageById",{id:id},"post");
    data=data.advantageModel;
    if(data!=null){
        $("#title").val(data.title);
        $("#adContent").val(data.content);
        $("#imgHref").attr("src",data.advantageImg);
        $("input[name='show'][value="+data.isShow+"]").attr("checked",true);
        $("input[name='toping'][value="+data.toping+"]").attr("checked",true);
    }

}

function advantageEdit() {
    let id=localStorage.getItem("advantageId");
    let data = new FormData();
    data.append("title", $("#title").val());
    data.append("adContent", $("#adContent").val());
    data.append("advantageImg", $("#advantageImg")[0].files[0]);
    data.append("show", $("input[name='show']:checked").val());
    data.append("toping", $("input[name='toping']:checked").val());
    data.append("id", id);

    let result=imgAjax("/back/advantage/advantageEdit",data,"post")

            if (result.count == 1) {
                alert("修改成功")
                $("#content").load("/back/advantage/advantageAll.html")
            } else {
                alert("修改失败");
            }

}