function Myajax(url,data,type) {

    let result=null;
    data.nameOne=localStorage.getItem("username");
    $.ajax({
        url:"http://localhost:8088"+url,
        data:data,
        type:type,
        async:false,
        xhrFields:{
            withCredentials:true//允许跨域带Cookies
        },
        dataType:'json',
        success:function (data) {
            result=data;
        }
    })

    return result;

}

function imgAjax(url,data,type) {
    let result=null;
    // data.append("nameOne",localStorage.getItem("username"));
    $.ajax({
        type: type,
        url: "http://localhost:8088"+url+"?nameOne="+localStorage.getItem("username"),
        contentType: false,
        async:false,
        data: data,
        processData: false,
        dataType: "json",
        xhrFields:{
            withCredentials:true//允许跨域带Cookies
        },
        success: function (data) {

            result=data;
        }
    });
    return result

}
