$(function () {
    getAboutUsInfo()
});

function getAboutUsInfo() {

    let data = Myajax("/aboutus", {}, "get")
    console.log(data);
    setAboutUs(data.aboutUsModel);
    setAdvantage(data.advantageModelList);
    setProjectType(data.projectTypeModelList);
    setProjectRotation(data.projectModelList);
    setAboutUsNavs(data.projectTypeModelNavList);
}

function setAboutUsNavs(data) {
    let html='';
    let href="/Afront/project.html";
    for (let i=data.length-1;i>=0;i--){
        html+='<a href="'+href+'">\n' +
            '<div onclick="projectTypeJump('+data[i].id+')">'+data[i].typename +'</div>\n' +
            '</a>';
    }
    $("#projectTypeNavs").html(html);
}

//页面跳转将typeId存入本地存储;
function projectTypeJump(i) {
    localStorage.setItem("aboutUsProjectTypeId",i);
}

//对关于我们进行编写
function setAboutUs(data) {
    $("#aboutUsImg").attr("src", data.aboutUsImg);
    $("#abUsContent").html(data.abUsContent);
}

//优势设置
function setAdvantage(data) {

    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<div>\n' +
            '<img src="' + data[i].advantageImg + '" height="50" width="50"/>\n' +
            '<p><strong>' + data[i].title + '</strong></p>\n' +
            '<p>' + data[i].content + '</p>\n' +
            '</div>'
    }
    $("#advantage").html(html)

}

//项目类型数据设置
function setProjectType(data) {
    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<div>\n' +
            '<div><img src="' + data[i].typeImg + '" height="100" width="100"/></div>\n' +
            '<div><strong>' + data[i].typename + '</strong></div>\n' +
            '<div>\n' +
            '<p>' + data[i].typeIntroduce + '</p>\n' +
            '</div>\n' +
            '</div>'
    }

    $("#projectType").html(html)

}

//项目轮播设置
function setProjectRotation(data) {

    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += ' <img src="' + data[i].img + '" height="100" width="600"/>'
    }
    $("#projectRotation").html(html)
}