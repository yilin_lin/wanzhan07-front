$(function () {
    getProjectTypeForOnline()
});

function getProjectTypeForOnline() {

    let data=Myajax("/online",{},"get");
    data=data.projectTypeModelNavList;
    findProjectType(data)
    setOnlineNavs(data);
}

function setOnlineNavs(data) {
    let html='';
    let href="/Afront/project.html";
    for (let i=data.length-1;i>=0;i--){
        html+='<a href="'+href+'">\n' +
            '<div onclick="projectTypeJump('+data[i].id+')">'+data[i].typename +'</div>\n' +
            '</a>';
    }
    $("#projectTypeNavs").html(html);

}

//页面跳转将typeId存入本地存储;
function projectTypeJump(i) {
    localStorage.setItem("onlineProjectTypeId",i);
}

//将项目类型显示在留言新增页面
function findProjectType(data) {

    if (data!=null){
        let html="";
        for (let i=0;i<data.length;i++){
            html+='<input type="radio" name="consultantsProjectType" value="'+data[i].id+'">'+data[i].typename;
        }
        $("#consultantsProjectType").html(html);
    }

}

//添加在线留言信息
function consultantService() {
    let data={};
    data.projectType=$("input[type='radio'][name='consultantsProjectType']").val();
    data.consultants=$.trim($("#consultants").val());
    data.consultantsPhone=$.trim($("#consultantsPhone").val());
    data.consultantsEmail=$.trim($("#consultantsEmail").val());
    data.consultantsContent=$.trim($("#consultantsContent").val());
    let result=Myajax("/onlineConsultant",data,"post");
    if (result.count==1){
        alert("提交成功");
        location.reload();
    }else{
        alert("提交失败")
    }

}