let aboutUs=localStorage.getItem("aboutUsProjectTypeId");
let personnel=localStorage.getItem("personnelProjectTypeId");
let contact=localStorage.getItem("contactProjectTypeId");
let online=localStorage.getItem("onlineProjectTypeId")
$(function () {
    if (aboutUs!=null) {
        getProjectInfo(null,aboutUs);
        localStorage.removeItem("aboutUsProjectTypeId");
    }else if (personnel!=null) {
        getProjectInfo(null,personnel);
        localStorage.removeItem("personnelProjectTypeId");
    }else if (contact!=null){
        getProjectInfo(null,contact);
        localStorage.removeItem("contactProjectTypeId");
    } else if (online!=null){
        getProjectInfo(null,online);
        localStorage.removeItem("onlineProjectTypeId");
    } else {
        getProjectInfo();
    }


});

function getProjectInfo(pageNumber,projectTypeId) {
    let data=Myajax("/project",{pageNumber:pageNumber},"get")
    setProject(data.projectModelList,data.projectTotalCount);
    if (projectTypeId!=undefined){
        setProductNavs(data.projectTypeModelNavList,projectTypeId);
    }else{
        setProductNavs(data.projectTypeModelNavList);
    }


}
//项目设置
function setProject(data,projectTotalCount) {
    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += ' <div>\n' +
            '<div><img src="' + data[i].img + '" height="170" width="280"/></div>\n' +
            '<p>' + data[i].projectName + '</p>\n' +
            '</div>'
    }
    $("#project").html(html)
    pageShowProject(projectTotalCount);
}

//总共项目分页显示显示
function pageShowProject(Total) {
    let totalCount=parseInt(Total)
    let  pageNumber=Math.ceil(totalCount/8);
    let pageHtml="<div>&lt;</div>";
    for (let i = 1;i<=pageNumber;i++){
        pageHtml+='<div onclick="getProjectInfo('+i+',undefined)">'+i+'</div>';
    }
    pageHtml+='<div>&gt;</div>';
    $("#page").html(pageHtml);

}


//页面类型导航
function setProductNavs(data,projectTypeId) {
    let html='';
    for (let i=data.length-1;i>=0;i--){
        html+='<a title="'+data[i].id+'" >\n' +
            '<div onclick="projectTypeClick('+data[i].id+')">'+data[i].typename+'</div>\n' +
            '</a>';

    }

    $("#projectTypeNavs").html(html);


    if(projectTypeId!=undefined){
        projectTypeClick(projectTypeId,null,projectTypeId );
    }
}

//根据项目类型查找项目
function projectTypeClick(id,pageNum,projectTypeId) {
    let data=Myajax("/projectFindByType",{id:id,pageNumber:pageNum},"post");
    let totalCount=data.count;
    data=data.list;
    let html="";
    if (data!=null) {
        for (let i=0;i<data.length;i++){
            html += '<div>\n' +
                '<div><img src="' + data[i].img + '" height="170" width="280"/></div>\n' +
                '<p>' + data[i].projectName + '</p>\n' +
                '</div>'
        }
    }

    if (projectTypeId!=undefined) {
        $("a[title='"+projectTypeId+"']").css({"background-color": "#6bb90e", "color": "#f3f3f3"}).siblings().css({
            "background-color": "#f3f3f3",
            "color": "#000000"
        });
    }else{
        $("a[title='"+id+"']").css({"background-color": "#6bb90e", "color": "#f3f3f3"}).siblings().css({
            "background-color": "#f3f3f3",
            "color": "#000000"
        });
    }


    $("#project").html(html);
    pageShow(totalCount,id);
    $("#projectTypeName").html("&gt;&gt"+data[0].projectType);

}

//点击项目进行分页
// function projectclick(e, id) {
    // $(e).siblings().addClass("clearback");
    // let projectTyprid = id;

    // page("aa");
// }

//项目类别的分页功能展示
function pageShow(Total,projectTypeId) {
    let totalCount=parseInt(Total)
    let  pageNumber=Math.ceil(totalCount/8);
    let pageHtml="<div>&lt;</div>";
    for (let i = 1;i<=pageNumber;i++){
        pageHtml+='<div onclick="projectTypeClick('+projectTypeId+','+i+')">'+i+'</div>';
    }
    pageHtml+='<div>&gt;</div>';
    $("#page").html(pageHtml);
}


