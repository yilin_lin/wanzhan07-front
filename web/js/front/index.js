$(function () {
    indexFind();
})

function indexFind() {
    let data = Myajax("/index", "", "get");
    console.log(data);

    setProjectType(data.projectTypeModelList);
    setAdvantage(data.advantageModelList);
    setProject(data.projectModelList);
    setAdvertiseBanner(data.advertisementModel.bannerAd);
    setNews(data.newsModelList);
}




//项目类型数据设置
function setProjectType(data) {
    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<div>\n' +
            '<div><img src="' + data[i].typeImg + '" height="100" width="100"/></div>\n' +
            '<div><strong>' + data[i].typename + '</strong></div>\n' +
            '<div>\n' +
            '<p>' + data[i].typeIntroduce + '</p>\n' +
            '</div>\n' +
            '</div>'
    }

    $("#projectType").html(html)

}

//优势设置
function setAdvantage(data) {

    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<div>\n' +
            '<img src="' + data[i].advantageImg + '" height="50" width="50"/>\n' +
            '<p><strong>' + data[i].title + '</strong></p>\n' +
            '<p>' + data[i].content + '</p>\n' +
            '</div>'
    }
    $("#advantage").html(html)

}

//项目设置
function setProject(data) {

    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += ' <div>\n' +
            '<div><img src="' + data[i].img + '" height="170" width="280"/></div>\n' +
            '<p>' + data[i].projectName + '</p>\n' +
            '</div>'
    }
    $("#project").html(html)
}

//横幅广告设置
function setAdvertiseBanner(data) {
    $("#bannerAd").html(data);
}

//新闻信息设置
function setNews(data) {

    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<div>\n' +
            '<div>\n' +
            '<div><strong>'+data[i].id +'</strong></div>\n' +
            '<div><p>'+data[i].newsDate +'</p></div>\n' +
            '</div>\n' +
            '\n' +
            '<div>\n' +
            '<div><strong>'+data[i].newsTitle +'</strong></div>\n' +
            '<div>\n' +
            '<p>'+data[i].newsSummary +'</p>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>'
    }
    $("#news").html(html);

}