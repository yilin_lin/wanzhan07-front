$(function () {
    getNewsInfo();
});

function getNewsInfo() {
    let data=Myajax("/news",{},"get");
    console.log(data);
    setNewsNav(data.newsNavList);
    setNews(data.newsModelList);
}

//设置新闻页面的导航信息
function setNewsNav(data) {
    let html='';
    for (let i=data.length-1;i>=0;i--){
        html+='<a>\n' +
            '<div>'+data[i].typename +'</div>\n' +
            '</a>';
    }
    $("#projectTypeNavs").html(html);
}

//新闻信息设置
function setNews(data) {

    let html = '';
    for (let i = 0; i < data.length; i++) {
        html += '<div>\n' +
            '<div>\n' +
            '<div><strong>'+data[i].id +'</strong></div>\n' +
            '<div><p>'+data[i].newsDate +'</p></div>\n' +
            '</div>\n' +
            '\n' +
            '<div>\n' +
            '<div><strong>'+data[i].newsTitle +'</strong></div>\n' +
            '<div>\n' +
            '<p>'+data[i].newsSummary +'</p>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>'
    }
    $("#news").html(html);

}