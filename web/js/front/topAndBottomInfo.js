$(function () {
    getTopAndBottomInfo()
});

function getTopAndBottomInfo() {
    let data=Myajax("/topAndBottom","","get");
    setCompanyInfo(data.companyModel);
    setNavInfo(data.navModelList);
    setAdvertiseIndexAd(data.advertisementModel.indexTopAd)
    setAdvertiseTopAd(data.advertisementModel.topAd)
}

//公司信息的设置
function setCompanyInfo(data) {
    $("#logoPicture").attr("src", data.logo);
    $("#webname").html(data.webName);
    $("#emailPicture").attr("src", data.emailPicture);
    $("#email").html(data.email);
    $("#phonePicture").attr("src", data.phonePicture);
    $("#phone").html(data.phone);
    $("#telPhone").html(data.telPhone);
    $("#transfer").html(data.transfer);
    $("#address").html(data.address);
    $("#emailBottom").html(data.email);
    $("#telPhoneBottom").html(data.telPhone);
    $("#telPhoneBottom1").html(data.telPhone);
    $("#phoneBottom").html(data.phone);

//    联系我们
    $("#addressContact").html(data.address);
    $("#emailContact").html(data.email);
    $("#phoneContact").html(data.phone);
    $("#telPhoneContact").html(data.telPhone);
    $("#telPhoneContact1").html(data.phone);
}

//网页顶部和底部导航
function setNavInfo(data) {
    let html='';
    for(let i=0;i<data.length;i++){
        html+='<div><a href="'+data[i].navHref+'">'+data[i].navName+'</a></div>'
    }

    html+='<div class="search">\n' +
        '<div class="foleft">\n' +
        '<input type="text" value="請輸入關鍵字搜索">\n' +
        '<img src="../image/fdj1.png" height="28" width="30"/>\n' +
        '</div>\n' +
        '</div>'
    $("#topNav").html(html);

    let bhtml=''
    for(let i=0;i<data.length;i++){
        bhtml+='<a href="'+data[i].navHref+'"><div>'+data[i].navName+'</div></a>'
    }
    $("#navBottom").html(bhtml);
}


//首页顶部广告
function setAdvertiseIndexAd(data) {

    $("#indexTopAd").html(data);

}

//子页顶部广告
function setAdvertiseTopAd(data) {
    $("#topAd").html(data);
}