$(function () {
  getPersonnelInfo();
});

function getPersonnelInfo() {
    let data=Myajax("/personnel",{},"get")
    console.log(data);
    setRecruitAd(data.advertisementModel.recruitAd);
    setPersonnel(data.personnelModelList);
    setRecruitNavs(data.projectTypeModelNavList);
}

//设置招聘页面的广告信息
function setRecruitAd(data) {
    $("#recruitAd").html(data);
}

//设置招聘页面项目类型的导航信息
function setRecruitNavs(data) {
    let html='';
    let href="/Afront/project.html";
    for (let i=data.length-1;i>=0;i--){
        html+='<a href="'+href+'">\n' +
            '<div onclick="projectTypeJump('+data[i].id+')">'+data[i].typename +'</div>\n' +
            '</a>';
    }
    $("#projectTypeNavs").html(html);
}

//页面跳转将typeId存入本地存储;
function projectTypeJump(i) {
    localStorage.setItem("personnelProjectTypeId",i);
}

//设置招聘页面的招聘信息
function setPersonnel(data) {
  let html='';
    for (let i=0;i<data.length;i++){
        html+='<div>\n' +
            '<div><strong>招聘职位：'+data[i].position+'</strong></div>\n' +
            '<div>'+data[i].description +
            '</div>\n' +
            '<div>\n' +
            '<p>薪&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp资：'+data[i].salary+'</p>\n' +
            '<p>地&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp址：'+data[i].address+'</p>\n' +
            '<p> 联系电话: '+data[i].phone+'</p>\n' +
            '<p>简历发至: '+data[i].email+'</p>\n' +
            '</div>\n' +
            '</div>'
    }
    $("#personnel").html(html)
}
