$(function () {
    getContactInfo();
});

function getContactInfo(){
    let data=Myajax("/contact",{},"get");
    console.log(data)
    setContactNavs(data.projectTypeModelNavList);
}

//设置招聘页面项目类型的导航信息
function setContactNavs(data) {
    let html='';
    let href="/Afront/project.html";
    for (let i=data.length-1;i>=0;i--){
        html+='<a href="'+href+'">\n' +
            '<div onclick="projectTypeJump('+data[i].id+')">'+data[i].typename +'</div>\n' +
            '</a>';
    }
    $("#projectTypeNavs").html(html);
}

//页面跳转将typeId存入本地存储;
function projectTypeJump(i) {
    localStorage.setItem("contactProjectTypeId",i);
}