$(function () {
   $("#page").load("/back/page.html");
});

function findAll() {
    let da={};
    da.consultants = $("#consultants").val();
    da.consultantsPhone = $("#consultantsPhone").val();
    da.startBirthday = $("#startBirthday").val();
    da.endBirthday = $("#endBirthday").val();
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());

    let data=Myajax("/back/online/onlineAll",da,"get");
    $("#totalCount").html(data.count);
    data=data.list;
    console.log(data);
    if(data!=null){
        let html="";
        for(let i=0;i<data.length;i++){
            html+='<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].consultants + '</td>\n' +
                '<td>' + data[i].typeName + '</td>\n' +
                '<td>' + data[i].consultantsPhone + '</td>\n' +
                '<td>' + data[i].consultantsEmail + '</td>\n' +
                '<td>' + data[i].consultantsContent + '</td>\n' +
                '<td>' + (data[i].createTime == undefined ? ' ' : data[i].createTime) + '</td>\n' +
                '<td> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n' +
                '</tr>';
        }
        $("#onlineFindAll").html(html);

    }

}

function del(id) {

    let data=Myajax("/back/online/del",{id:id},"post")

    if (data.count==1){
        alert("删除成功");
        findAll()
    } else {
        alert("删除失败");
    }

}