$(function () {
    findPage('first')
});


function findPage(type) {

    let pageNumber=$("#pageNumber").val()==""?1:parseInt($("#pageNumber").val());
    let pageSize=$("#pageSize").val()==""?10:parseInt($("#pageSize").val());
    let totalCount=parseInt($("#totalCount").html());
    let totalPage=Math.ceil(totalCount/pageSize);

    switch (type) {
        case 'go':

            if(pageNumber>=totalPage){
                pageNumber=totalPage;
            }
            break;
        case 'first':
            pageNumber=1;
            break;
        case 'last':
            if(pageNumber<=1){
                pageNumber=1;
            }else{
                --pageNumber;
            }

            break;
        case 'next':
            if (pageNumber>=totalPage){
                pageNumber=totalPage;
            }else{
                ++pageNumber;
            }

            break;
        case 'end':
            pageNumber=totalPage;
            break;
    }
    $("#pageNumber").val(pageNumber);
    $("#pageSize").val(pageSize);
    findAll();


}
