$(function () {
    findCompanyById()
});

function findCompanyById() {
    let id=localStorage.getItem("companyId")
    let data=Myajax("/back/company/findCompany",{id:id},"post")
    data=data.companyModel
    if (data!=null){
        $("#webName").val(data.webName);
        $("#logoHref").attr("src",data.logo);
        $("#email").val(data.email);
        $("#emailHref").attr("src",data.emailPicture);
        $("#phone").val(data.phone);
        $("#telPhone").val(data.telPhone);
        $("#phoneHref").attr("src",data.phonePicture);
        $("#address").val(data.address);
        $("#transfer").val(data.transfer);
        $("input[name=show][value="+data.isShow+"]").attr("checked",true);
        $("input[name=toping][value="+data.toping+"]").attr("checked",true);
    }

}

function  companyEdit() {
    let id=localStorage.getItem("companyId");
    let data = new FormData();
    data.append("webName", $.trim($("#webName").val()));
    data.append("logo",  $("#logo")[0].files[0]);
    data.append("email", $.trim($("#email").val()));
    data.append("emailPicture",$("#emailPicture")[0].files[0]);
    data.append("phone",$.trim($("#phone").val()));
    data.append("telPhone",$.trim($("#telPhone").val()));
    data.append("phonePicture", $("#phonePicture")[0].files[0]);
    data.append("address", $.trim($("#address").val()));
    data.append("transfer", $.trim($("#transfer").val()));
    data.append("show", $("input[name='show']:checked").val());
    data.append("toping", $("input[name='toping']:checked").val());
    data.append("id",id);
    let result=imgAjax("/back/company/edit",data,"post")

            if (result.count == 1) {
                alert("�޸ĳɹ�")
                $("#content").load("/back/company/companyAll.html")
            } else {
                alert("�޸�ʧ��");
            }


}