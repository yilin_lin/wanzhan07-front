$(function () {
    getData();
})

function getData() {

    let id=localStorage.getItem("userid");
    let data=Myajax("/back/user/findById",{id:id},"post");
    data=data.userModel;
    console.log(data);
    if (data!=null){

        $("#username").val(data.username);
        $("#password").val(data.password);
        $("#phone").val(data.phone);
        $("input[name=sex][value=" + data.sex + "]").attr("checked", true);
        $("#birthday").val(data.birthday);

        $('input:checkbox[name=hobby]').each(function () {
            let string = data.hobby;//需要将后台拿到的爱好转换为数组
            console.log(string);
            //后台传递过来的值为[羽毛球, 乒乓球, 篮球],所以需要将中括号去掉,然后用字符的方法split转换为数组
            string = string.replace("[", "").replace("]", "");
            let hobby = string.split(",");
            for (let i = 0; i < hobby.length; i++) {
                console.log($(this).val(),$.trim(hobby[i]))
                if ($(this).val() == $.trim(hobby[i])) {
                    $(this).attr('checked', true);
                    break;
                }
            }
        });

        $("#introduce").val(data.introduce);

    }
}

function modify() {
    let id=localStorage.getItem("userid");
    let data = {
        id:id,
        username: $("#username").val(),
        password: $("#password").val(),
        phone: $("#phone").val(),
        sex: $("input[name='sex']:checked").val(),
        birthday: $("#birthday").val(),
        hobby: JSON.stringify(aihao()),
        introduce: $("#introduce").val()
    };
    let result=Myajax("/back/user/edit",data,"post")

            if (result.count==1) {
                alert("修改成功");
                $("#content").load("/back/user/userManagement.html");
            }

}

function aihao() {
    let hb = [];
    $("input:checkbox[name='hobby']").each(function () {
        console.log();
        if ($(this).prop("checked")) {
            hb.push($(this).val());
        }
    })
    return hb;
}