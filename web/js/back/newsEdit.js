$(function () {
    findNewsById()
});


function findNewsById() {
    let id=localStorage.getItem("newsId");
    let data=Myajax("/back/news/findNewsById",{id:id},"post");
    data=data.newsModel;
    if(data!=null){
        $("#newsTitle").val(data.newsTitle);
        $("#newsSummary").val(data.newsSummary);
        $("#newsDate").val(data.newsDate);
        $("input[name=show][value="+data.isShow+"]").attr("checked",true);
        $("input[name=toping][value="+data.toping+"]").attr("checked",true);
    }
}

function newsEdit() {
    let id=localStorage.getItem("newsId");
    let data={};
    data.id=id;
    data.newsTitle=$.trim($("#newsTitle").val());
    data.newsSummary=$.trim($("#newsSummary").val());
    data.newsDate=$.trim($("#newsDate").val());
    data.isShow=$("input[name='show']:checked").val();
    data.toping=$("input[name='toping']:checked").val();

    let result=Myajax("/back/news/newsEdit",data,"post")
    if(result.count==1){
        alert("修改成功");
        $("#content").load("/back/news/newsAll.html")
    }else{
        alert("修改失败")
    }

}