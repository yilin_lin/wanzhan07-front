$(function () {
    $("#page").load("/back/page.html");

})

function findAll() {

    let da={};
    da.navName=$.trim($("#navName").val());
    da.navHref=$.trim($("#navHref").val());
    da.isshow=$("input[name='isshow']:checked").val();
    da.toping=$("input[name='toping']:checked").val();
    da.startBirthday=$.trim($("#startBirthday").val());
    da.endBirthday=$.trim($("#endBirthday").val());
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());

    let data = Myajax("/back/navs/navAll", da, "get")
    console.log(data.count);
    $("#totalCount").html(data.count);
    data = data.list;
    let html = "";
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            console.log(data[i].updateTime);
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].navName + '</td>\n' +
                '<td>' + data[i].navHref + '</td>\n' +
                '<td>' +( data[i].isShow==1?'是':'否')+ '</td>\n' +
                '<td>' +( data[i].toping==1?'是':'否')+ '</td>\n' +
                '<td>' + data[i].creataTime + '</td>\n' +
                '<td>' + (data[i].updateTime ==undefined?' ':data[i].updateTime)+ '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n'
        }
        $("#navFindAll").html(html);
    }

}
function del(id) {

    let data=Myajax("/back/navs/del",{id:id},"post")
    if(data.count==1){
        alert("删除成功");
        findAll();
    }else{
        alert("删除失败");
    }

}

function edit(id) {
    localStorage.setItem("navId",id);
    $("#content").load("/back/navs/navEdit.html");

}