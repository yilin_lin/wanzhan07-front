$(function () {
    findAboutUsById();
});

function findAboutUsById() {

    let da={};
    da.id=localStorage.getItem("aboutUsId");

    let data=Myajax("/back/aboutUs/findAboutUsById",da,"post");
    data=data.aboutUsModel;
    if(data!=null){
        $("#abUsContent").val(data.abUsContent);
        $("#imgHref").attr("src",data.aboutUsImg);
        $("input[name='show'][value="+data.isShow+"]").attr("checked",true);
        $("input[name='toping'][value="+data.toping+"]").attr("checked",true);
    }

}

function aboutUsEdit() {
    let id=localStorage.getItem("aboutUsId");
    let data = new FormData();
    data.append("abUsContent", $("#abUsContent").val());
    data.append("aboutUsImg", $("#aboutUsImg")[0].files[0]);
    data.append("show", $("input[name='show']:checked").val());
    data.append("toping", $("input[name='toping']:checked").val());
    data.append("id", id);

    let result=imgAjax("/back/aboutUs/aboutUsEdit",data,"post");

            if (result.count == 1) {
                alert("修改成功")
                $("#content").load("/back/aboutUs/aboutUsAll.html")
            } else {
                alert("修改失败");
            }



}