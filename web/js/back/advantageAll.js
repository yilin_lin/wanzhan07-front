$(function () {
    $("#page").load("/back/page.html");
});

//通过id对数据进行排序
function orderById() {
    if ($("div[orderBy='id']").val()==0) {
        $("div[orderBy='id']").val("1");
        $("div[orderBy='id']").css("background-color","red")
    }else{
        $("div[orderBy='id']").val("0");
        $("div[orderBy='id']").css("background-color","white")
    }

    findAll();
}

function findAll() {
    let da={};
    da.advantageName=$.trim($("#advantageName").val());
    da.isshow=$("input[name='isshow']:checked").val();
    da.toping=$("input[name='toping']:checked").val();
    da.pageNumber = $.trim($("#pageNumber").val());
    da.pageSize = $.trim($("#pageSize").val());
    da.orderById = $("div[orderBy='id']").val();
    let data=Myajax("/back/advantage/advantageAll",da,"get");

        $("#totalCount").html(data.count);

    //
    data=data.list;
    if (data!=null) {
        let html = '';
        for (let i = 0; i < data.length; i++) {
            console.log(data[0].updateTime);
            html += '<tr>\n' +
                '<td>' + data[i].id + '</td>\n' +
                '<td>' + data[i].title + '</td>\n' +
                '<td><img src="' +data[i].advantageImg +'"style="width: 50px;height: 40px"/> </td>\n' +
                '<td>' + data[i].content + '</td>\n' +
                '<td>' + (data[i].isShow == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].toping == 1 ? '是' : '否') + '</td>\n' +
                '<td>' + (data[i].createTime == undefined ? ' ' : data[i].createTime) + '</td>\n' +
                '<td>' + (data[i].updateTime == undefined ? ' ' : data[i].updateTime) + '</td>\n' +
                '<td><a href="javascript:edit(' + data[i].id + ')">编辑</a> ' +
                '<a href="javascript:del(' + data[i].id + ')">删除</a></td>\n'
            '</tr>';
            $("#advantageFindAll").html(html);//对当前页面进行赋值
        }
    }
}





function del(id) {
    let data=Myajax("/back/advantage/del",{id:id},"post")
    if (data.count==1) {
        alert("删除成功")
        findAll();
    }else{
        alert("删除失败")
    }

}

function edit(id) {
    localStorage.setItem("advantageId",id);
    $("#content").load("/back/advantage/advantageEdit.html");

}