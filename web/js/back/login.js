function flushCode(object) {
    // document.getElementById("code").src = "/code?date=" + new Date();
    // object.src="/code?date=" + new Date();
    $(object).attr("src", "http://localhost:8088/code?date=" + new Date());
}

function checkUsername() {
    // let username=document.getElementById("username").value; js写法
    return checkText("username", /^[a-zA-Z]\w{4,15}$/, "usernameTip", "用户名输入格式正确", "用户名输入格式不对");
}


//检查密码格式
function checkPassword() {

    return checkText("password", /^[a-zA-Z]\w{4,15}$/, "passwordTip", "", "密码格式输入格式不对");

}

//检查文本格式
function checkText(id, regs, tip, success, error) {

    let name = $("#" + id).val();
    let reg = regs;
    // console.log(reg.test(name));
    if (reg.test(name)) {
        $("#" + tip).html(success);
        $("#" + tip).css("color", "green");
        return true;
    } else {
        $("#" + tip).html(error);
        $("#" + tip).css("color", "red");
        return false;
    }

}

//检查所有信息
function checkAll() {

    let username = $("#username").val();
    let password = checkPassword();


    if (true) {
        let data = {}
            data.username=username;
            data.password=$("#password").val();
            data.verification=$("#verification").val();

        let  result = Myajax("/login", data, "post");
        if (result.count == 1) {
            localStorage.setItem("username",username);
            console.log(localStorage.getItem("username"))
            alert("登录成功")
            window.open("/back/backHome.html");
        } else if (result.count == 0) {
            alert("登录失败");

        } else if (result.count == -1) {
            alert("验证码错误");

        }

    }else {
        alert("格式不正确,请重新输入");
    }

}