$(function () {
    findPersonnelById();
})
function findPersonnelById() {
    let id=localStorage.getItem("personnelId");
    let data=Myajax("/back/personnel/findPersonnelById",{id:id},"post");
    data=data.personnelModel;
    if(data!=null){
        $("#position").val(data.position);
        $("#description").val(data.description);
        $("#salary").val(data.salary);
        $("#address").val(data.address);
        $("#phone").val(data.phone);
        $("#email").val(data.email);
        $("input[name='isShow'][value="+data.isShow+"]").attr("checked",true)
        $("input[name='toping'][value="+data.toping+"]").attr("checked",true)
    }
}

function personnelEdit() {
    let id=localStorage.getItem("personnelId")
    let data={}
    data.id=id;
    data.position=$.trim($("#position").val());
    data.description=$.trim($("#description").val());
    data.salary=$.trim($("#salary").val());
    data.address=$.trim($("#address").val());
    data.phone=$.trim($("#phone").val());
    data.email=$.trim($("#email").val());
    data.isShow=$("input[name='isShow']:checked").val();
    data.toping=$("input[name='toping']:checked").val();

    let result=Myajax("/back/personnel/personnelEdit",data,"post")

    if (result.count==1){
        alert("修改成功")
        $("#content").load("/back/personnel/personnelAll.html")
    } else{
        alert("修改失败")
    }

}