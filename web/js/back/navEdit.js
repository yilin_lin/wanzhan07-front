$(function () {
    findNavById();
})


function findNavById() {

    let id=localStorage.getItem("navId")
    let data=Myajax("/back/navs/findNavById",{id:id},"post")
    data=data.navModel;
    if(data!=null){
        $("#navName").val(data.navName);
        $("#navHref").val(data.navHref);
        $("input[name=isshow][value="+data.isShow+"]").attr("checked",true);
        $("input[name=toping][value="+data.toping+"]").attr("checked",true);
    }
}

function navEdit(){
    let id=localStorage.getItem("navId")
    let data={
        id:id,
        navName:$.trim($("#navName").val()),
        navHref:$.trim($("#navHref").val()),
        isShow:$("input[name='isshow']:checked").val(),
        toping:$("input[name='toping']:checked").val()
    }

    let result=Myajax("/back/navs/edit",data,"post")
    if(result.count==1){
        alert("修改成功")
        $("#content").load("/back/navs/navAll.html")
    }else{
        alert("导航名已经存在")
    }
}