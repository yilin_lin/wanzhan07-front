function flushCode(object) {
    // document.getElementById("code").src = "/code?date=" + new Date();
    // object.src="/code?date=" + new Date();
    $(object).attr("src", "/code?date=" + new Date());
}
//检查用户名格式
function checkUsername() {
    // let username=document.getElementById("username").value; js写法
    return checkText("username", /^[a-zA-Z]\w{4,15}$/, "usernameTip", "用户名输入格式正确", "用户名输入格式不对");
}

//检查密码格式
function checkPassword() {

    let password = $("#password").val();
    let reg = /^[a-zA-Z]\w{4,15}$/;
    if (reg.test(password)) {
        $("#passwordTip").html("输入密码格式正确");
        $("#passwordTip").css("color", "green");
        return true;

    } else {
        $("#passwordTip").html("格式输入错误");
        $("#passwordTip").css("color", "red");
        return false;

    }

}

function checkPassword1() {

    let password1 = $("#password1").val();
    let password = $("#password").val();
    if (password1 == password) {
        $("#passwordTip1").html("两次密码输入一致")
        $("#passwordTip1").css("color", "green");
        return true;

    } else {
        $("#passwordTip1").html("两次密码不一致");
        $("#passwordTip1").css("color", "red");
        return false;

    }

}

//检查电话格式
function checkPhone() {

    return checkText("phone", /^1([38][0-9]|4[5-9]|5[0-3,5-9]|66|7[0-8]|9[89])[0-9]{8}$/, "phoneTip", "电话格式正确", "电话格式不对");

}

//检查生日


//检查文本格式
function checkText(id, regs, tip, success, error) {

    let name = $("#" + id).val();
    let reg = regs;
    // console.log(reg.test(name));
    if (reg.test(name)) {
        $("#" + tip).html(success);
        $("#" + tip).css("color", "green");
        return true;
    } else {
        $("#" + tip).html(error);
        $("#" + tip).css("color", "red");
        return false;
    }

}

//检查所有信息
function checkAll() {

    let name = checkUsername();
    // let password=checkPassword();
    // let password1=checkPassword1();
    // let phone=checkPhone();


    if (name) {
        let data = {
            username: $("#username").val(),
            password: $("#password").val(),
            password1: $("#password1").val(),
            verification: $("#verification").val(),
            phone: $("#phone").val(),
            sex: $("input[name='sex']:checked").val(),
            birthday: $("#birthday").val(),
            hobby: JSON.stringify(aihao()),
            introduce: $("#introduce").val()
        };
        $.ajax({
            url: '/userRegister',//一样和servelet里的注解
            data: data,// 传递到后台的值
            type: 'post',//发送数据的方法
            dataType: 'json',//后台传入值的类型text，json，html，application，json
            success: function (data) {
                console.log(data.count);

                if (data.count < 0) {
                    alert("验证码错误");
                } else {
                    if (data.count == 1) {
                        window.open("/login.html");
                    } else {
                        alert("注册失败:用户名已经存在");
                    }
                }


            }//后台传入前段的值
        })
    }

}

function aihao() {
    let hb = [];
    $("input:checkbox[name='hobby']").each(function () {
        console.log();
        if ($(this).prop("checked")) {
            hb.push($(this).val());

        }
    })
    return hb;

}